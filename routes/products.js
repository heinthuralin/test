const express = require('express')
const productController = require('../controllers/products')

const { body } = require('express-validator');

const router = express.Router();

router.get('/categories', 
    productController.getAllMainCategories);

router.post('/categories', 
    body('name').notEmpty(),
    body('description').notEmpty(),
    productController.createMainCategory);
 
router.put('/categories/:id', 
    body('name').notEmpty(),
    body('description').notEmpty(),
    productController.updateMainCategory);

router.delete('/categories/:id', 
    productController.deleteMainCategory);

router.get('/sub-categories', 
    productController.getAllSubCategories);

router.post('/sub-categories', 
    body('name').notEmpty(),
    body('description').notEmpty(),
    body('main_category_id').notEmpty(),
    productController.createSubCategory);
	
module.exports = router