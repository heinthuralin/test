const express = require('express')
const authController = require('../controllers/auth')

const { body } = require('express-validator');

const router = express.Router();

router.post('/login', 
    body('email').notEmpty(),
    body('email').isEmail(),
    body('password').notEmpty(), 
    authController.login); 
	
module.exports = router