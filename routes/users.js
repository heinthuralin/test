const express = require('express')
const userController = require('../controllers/users')

const router = express.Router();
const { body } = require('express-validator');

router.get('/all', userController.getAllUsers);
router.post('/create-user', 
    body('email').notEmpty(),
    body('name').notEmpty(),
    body('password').notEmpty(),
    userController.createUser);

router.delete('/delete', body('email').notEmpty(), userController.deleteUser);
	
module.exports = router