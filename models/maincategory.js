'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MainCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      MainCategory.hasMany(models.SubCategory, { 
        foreignKey: 'main_category_id', 
        as: 'SubCategories'
      });
    }
  }
  MainCategory.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'MainCategory',
    tableName: 'main_categories',
    paranoid: true
  });
  return MainCategory;
};