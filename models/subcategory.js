'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SubCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      SubCategory.belongsTo(models.MainCategory, { 
        foreignKey: 'main_category_id', 
        as: 'MainCategory'
      });
    }
  }
  SubCategory.init({
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    main_category_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'SubCategory',
    tableName: 'sub_categories',
    paranoid: true
  });
  return SubCategory;
};