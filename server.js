const http = require("http");
const express = require("express");

const userRoute = require("./routes/users");
const authRouter = require("./routes/auth");
const productRouter = require("./routes/products");

const authMiddleware = require("./middlewares/authmiddleware");

const port = 3000;
const app = express();

app.use(express.json({ limit: "100mb" }));
app.use(express.urlencoded({ limit: "100mb", extended: true }));


app.use("/users",userRoute);
app.use("/auth", authRouter);
app.use("/products",productRouter);


app.get("/", function(req, res) {
    res.send("Hello from Express");
});

const server = http.createServer(app);

server.listen(port);

server.on("listening", () => {
    console.log("Listening on port", port);
});
