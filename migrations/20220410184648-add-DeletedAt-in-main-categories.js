'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('main_categories', 'deletedAt', Sequelize.DATE, {
      allowNull: true,
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('main_categories', 'deletedAt');
  }
};
