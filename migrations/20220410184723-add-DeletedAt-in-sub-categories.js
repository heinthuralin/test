'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('sub_categories', 'deletedAt', Sequelize.DATE, {
      allowNull: true,
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('sub_categories', 'deletedAt');
  }
};
