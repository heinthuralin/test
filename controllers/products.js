
const User = require('../models').User;
const MainCategory = require('../models').MainCategory;
const SubCategory = require('../models').SubCategory;

const { validationResult } = require('express-validator');

const productController = {

	/// Start Main Category ///

    getAllMainCategories: async (req, res, next) => {
		await MainCategory.findAll({
			attributes: ["id","name", "description"]
        }).then(function (data) {
			return res.status(200).json({ success: true, data: data});
		}).catch(function (error) {
			res.status(500).json({ success: false, data: error }).end();
		});
    },

	createMainCategory: async (req, res, next) => {
		const validationErrors = validationResult(req);

	    if (!validationErrors.isEmpty()) {
	      	return res.status(400).json({ errors: validationErrors.array() });
	    }

		const user = req.headers.authUser;

		await MainCategory.create({
			name : req.body.name,
			description : req.body.description,
		}).then(function (data) {
			return res.status(200).json({ success: true, data: "successfully created"});
		}).catch(function (error) {
			res.status(500).json({ success: false, data: error }).end();
		});

	},

	updateMainCategory: async (req, res) => {
		const validationErrors = validationResult(req);

	    if (!validationErrors.isEmpty()) {
	      	return res.status(400).json({ errors: validationErrors.array() });
	    }

		await MainCategory.findOne( 
			{ where: { id: req.params.id }
		}).then(function (data) {
			if(data){
				data.update( {
					name : req.body.name,
					description : req.body.description,
				}).then(function () {
					return res.status(200).json({ success: true, data: 'Successessfully Updated'});
				}).catch(function (error) {
					res.status(500).json({ success: false, data: error }).end();
				});
			}else{
				res.status(404).json({ success: false, data: "Main Category not found" });
			}
		}).catch(function (error) {
			res.status(500).json({ success: false, data: error }).end();
		});
	},

	deleteMainCategory: async (req, res, next) => {
		await MainCategory.destroy({
			where: { id: req.params.id }
		}).then(function () {
			return res.status(200).json({ success: true, data: 'Successessfully Deleted'});
		}).catch(function (error) {
			res.status(500).json({ success: false, data: error }).end();
		});
	},
	/// End Main Category ///

	/// Start Sub Category ///

	getAllSubCategories: async (req, res, next) => {
		await SubCategory.findAll({
			attributes: ["name", "description"],
			include: [
                {
                    model: MainCategory,
                    as: "MainCategory",
					attributes: ["name"],
                },
            ],
        }).then(function (data) {
			return res.status(200).json({ success: true, data: data});
		}).catch(function (error) {
			res.status(500).json({ success: false, data: error }).end();
		});
    },

	createSubCategory: async (req, res, next) => {
		const validationErrors = validationResult(req);

	    if (!validationErrors.isEmpty()) {
	      	return res.status(400).json({ errors: validationErrors.array() });
	    }

		const user = req.headers.authUser;

		await SubCategory.create({
			name : req.body.name,
			description : req.body.description,
			main_category_id :req.body.main_category_id,
		}).then(function (data) {
			return res.status(200).json({ success: true, data: "Successfully Created"});
		}).catch(function (error) {
			res.status(500).json({ success: false, data: error }).end();
		});

	},

	/// End Sub Category ///
}

module.exports = productController