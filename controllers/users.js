const User = require('../models').User;
const { validationResult } = require('express-validator');

const bcrypt = require('bcrypt');
const saltRounds = 10;

const userController = {

    getAllUsers: async (req, res, next) => {

		await User.findAll({
			attributes: ["email","name"]
        }).then(function (data) {
			return res.status(200).json({ success: true, data: data});
		}).catch(function (error) {
			return res.status(500).json({ success: false, data: error }).end();
		});
    },

	createUser: async (req, res, next ) => {

		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		var myPlaintextPassword = req.body.password;

		var hashedPassword = null;

		await bcrypt.hash(myPlaintextPassword, saltRounds)
		.then(function(hash) {
			hashedPassword = hash;
		});

		await User.create({ 
			name: req.body.name, 
			email: req.body.email,
			password: hashedPassword
		}).then(function(data){
			return res.status(200).json({ success: true, data: data});
		}).catch(function (error){
			return res.status(500).json({ success: false, data: error }).end();
		})
	},
	
	deleteUser: async (req, res, next) => {

		const errors = validationResult(req);

		if(!errors.isEmpty()){
			return res.status(400).json({ errors: errors.array() });
		}
		
		await User.destroy({
			where: { email: req.body.email }
		});

		return res.status(200).json({ success: true, });

	}
}

module.exports = userController