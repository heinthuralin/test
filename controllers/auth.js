
const User = require('../models').User;

var jwt = require("jsonwebtoken");
require('dotenv').config()
const { validationResult } = require('express-validator');

const bcrypt = require('bcrypt');

const authController = {
	
	login: async (req, res, next) => {

		const errors = validationResult(req);
	    if (!errors.isEmpty()) {
	      	return res.status(400).json({ status: false, data: errors.array() });
	    }

        var user = await User.findOne({ where: {email: req.body.email }});

		if(user === null) {
			res.status(422).json({ status: false, data: "Invalid Credential" }).end();
		}else{

            const checkPass = await bcrypt.compare(req.body.password, user.password);

            if(checkPass) {
    
                const payload = {
                    userID: user.id,
                };

				const userData = {
					userName: user.username,
                    userType: user.user_type
				}
    
                const token = jwt.sign(payload, process.env.TOKEN_SECRET);
    
                return res.status(200).json({
                    success: true,
                    token: token,
					data: userData
                })
    
            }else{
                res.status(422).json({ status: false, data: "Invalid Credential" }).end();
            }
        }
	}
}

module.exports = authController





