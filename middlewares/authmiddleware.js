var jwt = require("jsonwebtoken");	
require('dotenv').config()

const AuthMiddleware = function (req, res, next) {

	//references form sayar ei maung

	const authHeader = req.headers["authorization"];

	if(!authHeader) return next(res.status(403).json({ success: false, data: "Invalid Token" }).end());

	const [ type, token ] = authHeader.split(" ");

	if(type !== "Bearer") res.status(401).json({ success: false, data: "Invalid Token Type" }).end();
	
		jwt.verify(token, process.env.TOKEN_SECRET, function(err,authorizedData) {
			if(err) {
				res.status(403).json({ success: false, data: "Invalid Token" }).end();
			}  
			else{
				req.headers.authUser = authorizedData;
				next();
			} 
	});
};

module.exports = AuthMiddleware